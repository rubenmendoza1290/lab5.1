module gitlab.com/rubenmendoza1290/cloudnative

go 1.19

require (
	gitlab.com/rubenmendoza1290/lab5 v0.0.0-20230215162734-83cd038a746e
	gitlab.com/rubenmendoza1290/lab5.1 v0.0.0-20230221215007-6d2659c43fcb
	google.golang.org/grpc v1.53.0
	google.golang.org/protobuf v1.28.1
)

require (
	github.com/golang/protobuf v1.5.2 // indirect
	golang.org/x/net v0.5.0 // indirect
	golang.org/x/sys v0.4.0 // indirect
	golang.org/x/text v0.6.0 // indirect
	google.golang.org/genproto v0.0.0-20230110181048-76db0878b65f // indirect
)
